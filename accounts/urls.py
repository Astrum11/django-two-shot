from django.urls import path
from accounts.views import login_user, logout_user, user_signup

urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", user_signup, name="signup"),
]
